# 코딩테스트

+ Javascript 세미나" 참석자 정보를 API 데이터로 제공
+ 동일한 데이터로 PC, Mobile에 서로 다른 UX 경험 제공

## 공통
+ 디자인 요소로 icon font 사용 (awsome font)
+ media quiry 사용하여 768px 이하 해상도에서 모바일 UX 제공

## 웹
+ slick slider plugin 사용하여 Carousel 형태 구현
+ 키보드 접근성 고려하여 custom
+ ie10까지 크로스 브라우징 완료

## 모바일
+ 모바일 해상도에서 unslick 후 아코디언 형태 구현
+ name 클릭시 해당 box offset으로 scroll animation 적용
