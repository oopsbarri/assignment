var winW = $(window).width(), moMaxW = 768;
var $userSlide = $('#userInformation'), delay = 200, timer = null, crrIdx;

//브라우저 width 가변시 설정
$(window).on('resize', function () {
    clearTimeout(timer);
    timer = setTimeout(function () {
        crrIdx = $userSlide.find('.slick-current').attr('data-slick-index');
        $userSlide.slick('slickGoTo', crrIdx);
    }, delay);
    winW = $(window).width();
    if(winW > moMaxW){
        $userSlide.find('.user_info').show();
        slideInit();
    } else {
        $userSlide.find('.user_info').hide();
        $userSlide.find('.name i').removeClass('fa-chevron-up');
    }
});

//mobile accodian
$('body').on('click', '#userInformation .box .name', function () {
    if (winW <= moMaxW) {
        var $this = $(this), thisOffset;

        $('.name').not($this).find('i').removeClass('fa-chevron-up');
        $this.find('i').toggleClass('fa-chevron-up');
        
        $('.name').not($this).next().slideUp(300);
        $this.next().slideToggle(300, function () {		
            thisOffset = $this.offset().top - 15;
            $('html, body').animate({ scrollTop: thisOffset }, 300);
        });
    }
})

//데이터 통신
$(function () {
    $.support.cors = true; // 크로스도메인 허용
    $.ajax({
        url: "https://jsonplaceholder.typicode.com/users",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data, function () {
                var data = "", suiteText = this.address["suite"], findString = "Suite";

                data += '<div class="box">';
                if (suiteText.indexOf(findString) != -1) {
                    data += '<div class="name suite"><p>' + this.name + '</p><i class="fas fa-chevron-down"></i></div>';
                }
                else {
                    data += '<div class="name"><p>' + this.name + '</p><i class="fas fa-chevron-down"></i></div>';
                }
                data +=     '<div class="user_info">';
                data +=         '<p class="company">' + this.company["name"] + '</p>';
                data +=         '<p class="email">' + this.email + '</p>';
                data +=         '<p class="phone">' + this.phone + '</p>';
                data +=         '<div class="bottom_info">';
                data +=             '<p class="address">' + this.address["street"] + '&nbsp;' + suiteText + '&nbsp;' + this.address["city"] + '&nbsp;' + this.address["zipcode"] + '</p>';
                data +=             '<a href="' + this.website + '" class="website" target="_blank" title="새창으로 열기">' + this.website + '</a>';
                data +=         '</div>';
                data +=     '</div>';
                data += '</div>';

                $userSlide.append(data);
            });
            slideInit();
        },
        error: function (request, status, error) {
            alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
        }
    });
});

//slide init
function slideInit(){
    $userSlide.not('.slick-initialized').slick({
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 769,
                settings: "unslick"
            }
        ]
    });
};